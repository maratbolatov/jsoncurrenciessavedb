package com.mbolatov.jsoncurrenciessavedb.botserver;


import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;


public class Main {

           public static void main(String[] args) {
            ApiContextInitializer.init();
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
            try {
                telegramBotsApi.registerBot(new CurrenciesCheckBot()); //Check
            } catch (TelegramApiRequestException e) {
                e.printStackTrace();
            }

           }

}



