package com.mbolatov.jsoncurrenciessavedb.botserver;


import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CurrenciesCheckBot extends TelegramLongPollingBot {

    private static final String START_MESSAGE_TEXT = "Здесь Вы можете узнать актуальные курсы валют";
    private static final String CUSTOM_KEYBOARD_TEXT = "Узнать курсы валют";
    private static final String DATA_BASE_URL = "jdbc:mysql://localhost:3306/currency";
    private static final String USER_NAME = "root";
    private static final String PASSWORD = null;
    private static final String SQL_QUERY_GET_CURRENCY = "SELECT* FROM currency.tbl_currencies";
    private String text;

    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        if(message!=null && message.hasText());
        switch (message.getText()) {
            case "/start":
                sendMsg(message, START_MESSAGE_TEXT);
                break;
            case CUSTOM_KEYBOARD_TEXT:
                List<Currency> currencies = readCurrency();
                text = formatCurrencies(currencies);
                sendMsg(message, text);
                break;

                default:
                    sendMsg(message, text);
        }

    }


    private String formatCurrencies(List<Currency> currencies) {
        StringBuilder builder = new StringBuilder();
        for (Currency currency : currencies) {
            builder.append(currency.getCodename());
            builder.append(':');
            builder.append(' ');
            builder.append(currency.getRate());
            builder.append(' ');
            builder.append('(');
            builder.append(currency.getName_rus());
            builder.append(')');
            builder.append("\n\r");
        }
        return builder.toString();
    }


    private void sendMsg(Message message, String s) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(s);
        try {
            setButtons(sendMessage);
            sendMessage(sendMessage);
        } catch (TelegramApiException e){
            e.printStackTrace();
        }
    }

    private void setButtons(SendMessage sendMessage){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        List<KeyboardRow> keyboardRowList = new ArrayList<>();
        KeyboardRow keyboardButtons = new KeyboardRow();
        keyboardButtons.add(new KeyboardButton(CUSTOM_KEYBOARD_TEXT));
        keyboardRowList.add(keyboardButtons);
        replyKeyboardMarkup.setKeyboard(keyboardRowList);
    }

    private List<Currency> readCurrency() {
        List<Currency> currencies = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DATA_BASE_URL, USER_NAME, PASSWORD)) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_QUERY_GET_CURRENCY);
            while (resultSet.next()){
                String name_rus = resultSet.getString("name_rus");
                String rate = resultSet.getString("rate");
                String codename = resultSet.getString("codename");
                Currency currency = new Currency(name_rus,codename,rate);
                currencies.add(currency);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return currencies;
    }


    @Override
    public String getBotUsername() {
        return "CurrenciesCheckBot";
    }

    @Override
    public String getBotToken() {
        return "769478878:AAEPv5shrFdJfV6_mI9dlimhE25PGnvXEr8";
    }
}
