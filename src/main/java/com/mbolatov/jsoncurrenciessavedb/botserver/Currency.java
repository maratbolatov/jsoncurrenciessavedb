package com.mbolatov.jsoncurrenciessavedb.botserver;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Currency {

    private String name_rus;
    private String codename;
    private String rate;


    public Currency() {


    }
}
